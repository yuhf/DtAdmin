## 更新日志
 
## 所用框架
1. SpringBoot 1.5.3.RELEASE
2. MyBatis-Plus 2.0.8
3. MyBatis 3.4.4
4. Spring 4.3.8.RELEASE
5. hibernate-validator 5.3.5.Final
6. Ehcache 3.3.1
7. Kaptcha 2.3.2
8. Fastjson 1.2.31
9. Shiro 1.4.0
10.Druid 1.0.31


## 联系方式
maillank@qq.com

## 效果图




![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/133046_0ba45ba7_448530.jpeg "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/133102_453967b5_448530.jpeg "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/133121_d686517e_448530.jpeg "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0402/133146_bf77cd0a_448530.jpeg "在这里输入图片标题")